"""
Il semblerait que ce soit un commentaire multilignes
"""

# déclarations
comparer = 50
comparateur = 5
tata, titi, tutu, tete, toto = 1, 2, 3, 4, 5
legumes = ["patate", "endive", "courgette"]
patate, endive, courgette = legumes
floated = float(9)
integered = int("5")
stringed = str(7)
avis = "Nul à chier"

# condition
if comparer>comparateur:
    # print("oui", comparer, est plus grand que, comparateur)
    print(f"oui {comparer} est plus grand que {comparateur}")
    
def maPremiereFonction():
    avis = "Super"
    print("ton avis est", avis)
    
def avisGlobale():
    global avis
    avis = "INCROYAAAAABLE"
     
# afficher dans le terminal
print("ceci est un test")
print(floated, integered, stringed)
print(type(floated), type(integered), type(stringed))
print(tata, titi, tutu, tete, toto)
print(patate, endive, courgette)

maPremiereFonction()
print("ton avis est", avis)
avisGlobale()
print("ton avis est", avis)
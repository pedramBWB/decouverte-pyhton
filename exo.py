from itertools import count
import math
import random

### EXO 1 ###
prenom = "Pierre"
age = 20 
majeur = True
compte_en_banque = 20135.384
amis = ["Marie", "Julien", "Adrien"]
parents = (
    "Marc",
    "Caroline"
)

print(prenom, age, majeur, compte_en_banque, amis, parents)

### EXO 2 ### 
# Trouver l'erreur

site_web = "yes"
print(site_web)

### EXO 3 et 4 skip par le prof ###

### EXO 5 ###
# solution 1
print("2 + 6 + 3")

# solution 2
a = 2
b = 6
c = 3

# sep= va permettre d'ajouter un élément séparteur entre chaque options du print
print(a,b,c, sep=" + ")

### EXO 6 ###

# Test le type de ma donnée en argument
def testType(arg):
    if type(arg) == str :
        print("La variable est une chaine de caractère")
    else:
        print("c'est NON !")

testType(prenom)
prenom = 0
testType(prenom)

# Extension ---> Faire une fonction qui laisse le choix à l'utilisateur de choisir le type qu'il veut

### EXO 7 ###
phrase = "Bonjour les potos"

# Remplace une chaine de caractère par une autre
new_phrase = phrase.replace("Bonjour", "Salut")

print(new_phrase)  

# Extension ----> demandez à l'utilisateur le nombre d'occurence qu'il veut retirer
# ----> lui demander ce qu'il veut enlever

### EXO 8 ###
sort_me = ["a", "r", "p", "d", "v", "m", "q", "l"]

# Permet de trier la chaine de caractère dans l'ordre croissant (soir alphabétique ici)
def triMaChaine(chaine):
    chaine.sort()
    print("Votre chaine est trié :",chaine)
    
triMaChaine(sort_me)

### EXO 9 ###

# calcul le volume d'une sphère
def calculSphere():
    rayon = input("Quel est le rayon de votre sphère ? (cm3) \n")
    return 4/3 * math.pi * int(rayon)** 3

#print(calculSphere())

### EXO 10 ###
# range() permet retourne une liste de nombre d'un point donné à un autre
# etendu = range(10, 101, 1)
# list() affiche en liste la donnée
# print(list(etendu))

### EXO 11 ###

# chiffre_list = range(2, 201, 2)
# print(list(chiffre_list))


### EXO 12 ###

# fait la somme d'un lancé de deux dés
def lancéDés():
    first_dé = random.randint(1, 6)
    second_dé = random.randint(1, 6)
    
    return first_dé + second_dé


# Extension --> simuler n lancés de dès puis faire la moyenne de ces lancés

# Lance n fois les dés puis en fait une moyenne 
def lancéNFoisDé():
    n_lancé = int(input("Combien de fois voulez-vous lancés le dé ? \n"))
    lancé_result = 0
    for x in range(0, n_lancé, 1):
        print("lancé n°", x)
        addition_lancé = lancéDés()
        lancé_result += addition_lancé
        print("score :", lancé_result)
    
    moyenne = lancé_result / n_lancé
    print("La moyenne de votre score est de :", moyenne)
    print("Nombre de lancé est de :", n_lancé)
    
# lancéNFoisDé()

### EXO 13 ###

phrase_recherche = "Voila les amis, la lettre a est a trouvé"
print("nombre de a :", phrase_recherche.count("a"))

# Trouve la lettre répétée le plus de fois
def frequenceLettre():
    lettres = set(phrase_recherche)
    n_max = 0
    winning_letters = ""
    for letters in lettres:
        n_found = phrase_recherche.count(letters)
        print("lettre :", letters)
        print("trouvé :", n_found)
        if n_found > n_max and letters != " ":
            n_max = n_found
            winning_letters = letters
    
    print("LA LETTRE QUI RESSORT LE PLUS EST :", winning_letters)
        
    
    
frequenceLettre()
        
### EXO 14 ###

# affiche les impairs
liste_tech = [1,2,3,4,5,6,7,8,9]
print(liste_tech[::2])
    
     
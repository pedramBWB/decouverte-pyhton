# Imports
from dataclasses import replace
import random

print(random.randrange(1, 10))
print(random.uniform(1.0, 10.0))

# Boucle
chaine = "ceci est une chaine de test définit sur true"
chaine_false = "Ceci est une chaine false"
slice_me = "COUPE MOI, COUPE MOI J'AI DIT !!!!!!"
db2_simu = "test               "

print(len(chaine))
for letter in chaine:
    print(letter)

# Condition
if "true" in chaine:
    print("true est présent dans la chaîne")
else:
    print("raté")
    
if "false" in chaine_false:
    print("false est présent dans la chaîne")
else:
    print("raté")
    
# retire les espaces en début et fin de string
print(db2_simu.strip())

# remplacer un caratère par un autre 
print(slice_me.replace("!", b"\U+1F60F")) # Caractère d'échapement nécéssaire pour utiliser le unicode